import os
import time
from typing import Dict

import geojson
import rasterio
from osgeo import ogr
from rasterio.io import DatasetReader
from rasterio.plot import show

from processor.enums import SpatialEnums
from processor.model import ShapeEntity


class Utils:
    @staticmethod
    def get_driver_fromfile(band: str) -> str:
        if band is None:
            return SpatialEnums.JP2_DRIVER.value
        if band.split(".")[-1] == SpatialEnums.JP2_EXTENSION.value:
            driver = SpatialEnums.JP2_DRIVER.value
            return driver

    @staticmethod
    def get_driver_fromreader(band: DatasetReader) -> str:
        if band is not None and band.driver is not None:
            return band.driver

    @staticmethod
    def plot_rasterio_imagery(file):
        with rasterio.open(file, driver=Utils.get_driver_fromfile(file)) as file_data:
            show(file_data)

    @staticmethod
    def get_imagery_band(supported_bands, directory):
        imageryFiles = os.listdir(directory)
        filesDict = {}
        for supported_band in supported_bands:
            if supported_band not in filesDict.keys():
                filesDict[supported_band] = []
        for imageryFile in imageryFiles:
            path = directory + imageryFile + "\\GRANULE\\"
            path_10m = path + os.listdir(path)[0] + "\\IMG_DATA\\R10m\\"
            files_10m = os.listdir(path_10m)
            for file in files_10m:
                for supportedBand in supported_bands:
                    if file.split("_")[2] == supportedBand:
                        jp2_filename = path_10m + file
                        filesDict[supportedBand].append(jp2_filename)

        return filesDict

    @staticmethod
    def get_filename(operation: str, bandname: str) -> str:
        timenow = time.strftime(SpatialEnums.TIMESTAMP_FORMAT.value)
        merged_filename = operation + "-" + timenow + "-" + bandname + "." + SpatialEnums.JP2_EXTENSION.value
        return merged_filename

    @staticmethod
    def get_identifier_number(identifier: str) -> int:
        if identifier.lower() == SpatialEnums.SHAPE_IDENTIFIER_COUNTRY.value.lower():
            return 0
        elif identifier.lower() == SpatialEnums.SHAPE_IDENTIFIER_STATE.value.lower():
            return 1
        elif identifier.lower() == SpatialEnums.SHAPE_IDENTIFIER_DISTRICT.value.lower():
            return 2
        elif identifier.lower() == SpatialEnums.SHAPE_IDENTIFIER_SUBDISTRICT.value.lower():
            return 3
        else:
            return -1

    @staticmethod
    def get_crs(crs_string: str) -> str:
        return crs_string.split(":")[0].upper() + ":" + crs_string.split(":")[1]

    @staticmethod
    def get_json_from_shape(shape: ShapeEntity) -> geojson.Polygon:
        return geojson.Polygon(coordinates=shape.coordinates[0]['coordinates'])

    @staticmethod
    def get_json_from_product(product: Dict) -> str:
        return ogr.CreateGeometryFromWkt(product['footprint']).ExportToJson()
