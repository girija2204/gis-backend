import enum


class SpatialEnums(enum.Enum):
    JP2_DRIVER = "JP2OpenJPEG"
    GEOJSON_DRIVER = "GeoJSON"

    JP2_EXTENSION = "jp2"
    GEOJSON_EXTENSION = "geojson"

    TIMESTAMP_FORMAT = "%Y%m%d%H%M%S"
    SHAPE_EXTENSION = "shp"
    TIFF_EXTENSION = "tif"

    SHAPE_IDENTIFIER_COUNTRY = "country"
    SHAPE_IDENTIFIER_STATE = "state"
    SHAPE_IDENTIFIER_DISTRICT = "district"
    SHAPE_IDENTIFIER_SUBDISTRICT = "subdistrict"

    READING_MODE = "r"
    WRITING_MODE = "w"

    MERGING_OPERATION = "merger"
    REPROJECTING_OPERATION = "reprojector"
    MASKING_OPERATION = "masked"
