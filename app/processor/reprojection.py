import rasterio.merge
from rasterio.warp import calculate_default_transform, reproject, Resampling

from processor.enums import SpatialEnums
from utils.gis_utils import Utils


class Reprojector:
    def reproject(self, file_to_reproject: str, to_crs: str, merged_filepath: str, merged_reprojected: str,
                  band: str) -> str:
        reprojected_filename = Utils.get_filename(SpatialEnums.REPROJECTING_OPERATION.value, bandname=band)
        with rasterio.open(merged_filepath + file_to_reproject) as source:
            transform, width, height = calculate_default_transform(source.crs, to_crs, source.width, source.height,
                                                                   *source.bounds)
            kwargs = source.meta.copy()
            kwargs.update({
                "crs": to_crs,
                "transform": transform,
                "width": width,
                "height": height
            })

            with rasterio.open(merged_reprojected + reprojected_filename, "w", **kwargs) as destination:
                reproject(
                    source=rasterio.band(source, 1),
                    destination=rasterio.band(destination, 1),
                    src_transform=source.transform,
                    src_crs=source.crs,
                    dst_crs=to_crs,
                    resampling=Resampling.nearest
                )
        return reprojected_filename
