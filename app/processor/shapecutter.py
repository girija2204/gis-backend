from typing import Dict

import fiona

from processor.enums import SpatialEnums
from processor.model import ShapeEntity
from utils.gis_utils import Utils


class ShapeCutter:
    @staticmethod
    def get_shape(location: str, identifier: str) -> ShapeEntity:
        """

        :param location:
        :param identifier:
        :rtype: ShapeEntity
        """
        identifier_number = Utils.get_identifier_number(identifier=identifier)
        shapefile_path = f'C:\\Users\\girij\\PycharmProjects\\other resources\\gis data\\IND_adm\\IND_adm{identifier_number}.shp '
        geometry: Dict or None = None
        new_shape: ShapeEntity or None = None
        with fiona.open(shapefile_path, SpatialEnums.READING_MODE.value) as shapefile:
            name = shapefile.name
            crs = shapefile.crs['init']
            driver = shapefile.driver
            path = shapefile.path
            schema = shapefile.schema
            for feature in shapefile:
                if feature['properties']['NAME_2'] == location:
                    geometry = feature['geometry']
                    break

        if geometry is not None:
            new_shape = ShapeEntity(name=name, crs=crs, driver=driver,
                                    path=path,
                                    schema=schema, coordinates=geometry)

        return new_shape
