from typing import Dict, Optional

import rasterio.mask
import rasterio.merge

from processor.enums import SpatialEnums
from processor.model import ShapeEntity
from processor.reprojection import Reprojector
from utils.gis_utils import Utils


class Masker:
    def mask(self, imagery_data: Dict[str, str], shape_data: ShapeEntity, merged_filepath: str, merged_reprojected: str,
             masked_filepath: str) -> Dict[str, Optional[str]]:
        masked_filename_dict = {}
        for supported_band in imagery_data.keys():
            masked_filename_dict[supported_band] = None
        for supported_band in imagery_data.keys():
            file = merged_filepath + imagery_data[supported_band]
            dataset = rasterio.open(file, mode="r", driver=Utils.get_driver_fromfile(file))
            file_tomask = imagery_data[supported_band]
            path_tomask = merged_filepath
            if shape_data.crs != Utils.get_crs(dataset.crs['init']):
                reprojector = Reprojector()
                file_tomask = reprojector.reproject(imagery_data[supported_band], shape_data.crs, merged_filepath,
                                                    merged_reprojected, band=supported_band)
                path_tomask = merged_reprojected

            mask_dataset = rasterio.open(path_tomask + file_tomask)
            out_image, out_transform = rasterio.mask.mask(mask_dataset, shape_data.coordinates, crop=True)
            out_meta = mask_dataset.meta

            out_meta.update({"driver": Utils.get_driver_fromreader(mask_dataset),
                             "height": out_image.shape[1],
                             "width": out_image.shape[2],
                             "transform": out_transform})
            masked_filename = Utils.get_filename(operation=SpatialEnums.MASKING_OPERATION.value,
                                                 bandname=supported_band)
            with rasterio.open(masked_filepath + masked_filename, "w", **out_meta) as dest:
                dest.write(out_image)
            masked_filename_dict[supported_band] = masked_filename

        return masked_filename_dict
