from typing import List, Dict


class ShapeEntity:
    def __init__(self, name: str, crs: str, driver: str, path: str, schema: Dict, coordinates: List):
        self.name = name
        self.crs = self.get_crs(crs)
        self.driver = driver
        self.path = path
        self.schema = schema
        self.coordinates = coordinates

    def get_crs(self, crs_string: str) -> str:
        return crs_string.split(":")[0].upper() + ":" + crs_string.split(":")[1]