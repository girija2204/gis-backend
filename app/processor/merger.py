from typing import List, Dict, Optional

import rasterio.merge

from processor.enums import SpatialEnums
from utils.gis_utils import Utils


class Merger:

    def merge_bands(self, bands_dictionary: Dict[str, List[str]], merged_file_path) -> Dict[str, Optional[str]]:
        supportedBands = bands_dictionary.keys()
        band_data_dict = {}
        merged_filename_dict = {}
        for supportedBand in supportedBands:
            band_data_dict[supportedBand] = []
            merged_filename_dict[supportedBand] = None
        for supportedBand in supportedBands:
            allfiles_forband = bands_dictionary.get(supportedBand)
            for file_in_band in allfiles_forband:
                band_data_dict.get(supportedBand).append(
                    rasterio.open(file_in_band, driver=Utils.get_driver_fromfile(file_in_band)))
        for supportedBand in supportedBands:
            merged_file, out_trans = rasterio.merge.merge((band_data_dict.get(supportedBand)))

            merged_meta = band_data_dict.get(supportedBand)[0].meta.copy()
            merged_meta.update({
                "driver": Utils.get_driver_fromreader(band_data_dict.get(supportedBand)[0]),
                "height": merged_file.shape[1],
                "width": merged_file.shape[2],
                "transform": out_trans,
                "crs": band_data_dict.get(supportedBand)[0].crs
            })

            merged_filename = Utils.get_filename(SpatialEnums.MERGING_OPERATION.value, bandname=supportedBand)

            with rasterio.open(merged_file_path + merged_filename, "w", **merged_meta) as destination:
                destination.write(merged_file)

            merged_filename_dict[supportedBand] = merged_filename

        return merged_filename_dict
