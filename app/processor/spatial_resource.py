import datetime
from datetime import date

from osgeo import ogr
from sentinel_app.enums import SpatialEnums
from sentinelsat.sentinel import SentinelAPI, read_geojson, geojson_to_wkt


class GISResource:
    def __init__(self):
        self.api = SentinelAPI('girija2204', 'Girija2204@', 'https://scihub.copernicus.eu/dhus')

    def query_sentinel_products(self, footprint, fromdate, todate):
        return self.api.query(footprint,
                              date=(fromdate, todate),
                              area_relation='Intersects',
                              platformname='Sentinel-2')

    def remove_intersecting_products(self, product_list):
        union_polygon = ogr.CreateGeometryFromWkt(product_list[0][1]['footprint'])
        products_to_combine = [product_list[0]]
        for product in product_list[1:]:
            poly = ogr.CreateGeometryFromWkt(product[1]['footprint'])
            if (poly.GetArea() - union_polygon.Intersection(poly).GetArea()) < 0.001:
                print(f"Product {product[0]} is not eligible for union")
            else:
                union_polygon = union_polygon.Union(poly)
                products_to_combine.append(product)
        return products_to_combine, union_polygon

    def remove_by_bruteforce(self, products_to_combine, union_polygon):
        productids_to_remove = []
        for i in range(0, len(products_to_combine)):
            temp = products_to_combine.copy()
            temp.remove(temp[i])
            temp_union = ogr.CreateGeometryFromWkt(temp[0][1]['footprint'])
            for temp_product in temp[1:]:
                temp_union = temp_union.Union(ogr.CreateGeometryFromWkt(temp_product[1]['footprint']))
            if temp_union.GetArea() == union_polygon.GetArea():
                print(f'We can remove {products_to_combine[i][0]} from the product list as well.')
                productids_to_remove.append(products_to_combine[i][0])
        for product_id in productids_to_remove:
            for product in products_to_combine:
                if product[0] == product_id:
                    products_to_combine.remove(product)
                    break
        return products_to_combine

    def get_polygons_for_queriedproducts(self, products):
        """
        This has to be used when we bring a set of products directly from query
        and then want to see the collection of polygons on geojson.io.
        """
        final_products_ids_list = list(products)
        outDriver = ogr.GetDriverByName(SpatialEnums.GEOJSON_DRIVER.value)
        outDataSource = outDriver.CreateDataSource('queried_products.geojson')
        outLayer = outDataSource.CreateLayer('queried_products.geojson', geom_type=ogr.wkbPolygon)
        for product_id in final_products_ids_list:
            poly = ogr.CreateGeometryFromWkt(products.get(product_id)['footprint'])

            featureDefn = outLayer.GetLayerDefn()
            outFeature = ogr.Feature(featureDefn)
            outFeature.SetGeometry(poly)
            outLayer.CreateFeature(outFeature)

    def get_polygons_for_listproducts(self, footprints_list):
        """
        This has to be used when we bring a list of Polygons/Multipolygons after converting the products dictionary
        into a dataframe and then getting all the Multipolygons as a list.
        We can see the collection of polygons on geojson.io.
        """
        outDriver = ogr.GetDriverByName(SpatialEnums.GEOJSON_DRIVER.value)
        outDataSource = outDriver.CreateDataSource('list_products.geojson')
        outLayer = outDataSource.CreateLayer('list_products.geojson', geom_type=ogr.wkbPolygon)
        for footprint in footprints_list:
            poly = ogr.CreateGeometryFromWkt(footprint)

            featureDefn = outLayer.GetLayerDefn()
            outFeature = ogr.Feature(featureDefn)
            outFeature.SetGeometry(poly)
            outLayer.CreateFeature(outFeature)

    def download_products(self, products_list):
        for index, product in enumerate(products_list):
            print(f'Downloading product: {index + 1} of {len(products_list)}')
            self.api.download(id=product[0],
                              directory_path="C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\downloaded")

    def get_sentinel_products(self, footprint, fromdate, todate):
        products = self.query_sentinel_products(footprint, fromdate, todate)
        product_df = self.api.to_dataframe(products)

        # polygons_list = [product_df.footprint.values[1]] # do this if you want to get a single polygon
        footprints_list = product_df.footprint.values[:2].tolist()  # do this if you want a list of polygons
        self.get_polygons_for_listproducts(footprints_list=footprints_list)

        product_df.sort_values(['cloudcoverpercentage', 'ingestiondate'], ascending=[True, True], inplace=True)
        startDate = product_df.head(1)['beginposition'][0].date() - datetime.timedelta(days=4)
        endDate = product_df.head(1)['beginposition'][0].date() + datetime.timedelta(days=5)
        products = self.query_sentinel_products(footprint, startDate, endDate)

        self.get_polygons_for_queriedproducts(products=products)

        product_list = list(products.items())

        products_to_combine, union_polygon = self.remove_intersecting_products(product_list=product_list)
        footprints_list_intermediate = [product[1]['footprint'] for product in products_to_combine]
        self.get_polygons_for_listproducts(footprints_list_intermediate)

        products_to_combine = self.remove_by_bruteforce(products_to_combine.copy(), union_polygon)
        footprints_list_intermediate = [product[1]['footprint'] for product in products_to_combine]
        self.get_polygons_for_listproducts(footprints_list_intermediate)

        self.download_products(products_to_combine)

        # od = OrderedDict()
        # od[product_df.uuid[0]] = products.get(product_df.uuid[0])
        # json_product = self.api.to_geojson(od)
        # return json_product


resource = GISResource()
map_geojson_file = "../../../spatialApp/spatApps/sentinel_app/map.geojson"
footprint = geojson_to_wkt(read_geojson(map_geojson_file))
resource.get_sentinel_products(footprint, date(2020, 1, 1), date(2020, 6, 30))
