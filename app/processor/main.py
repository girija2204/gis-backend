
import logging.config
import os

import numpy as np
import rasterio
from rasterio.plot import show

from processor.merger import Merger
from processor.masking import Masker
from processor.shapecutter import ShapeCutter
from utils.gis_utils import Utils

print('hello django')
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CONFIG_DIR = BASE_DIR + "\\config"
CONFIG_FILE = CONFIG_DIR + "\\logging.conf"
logging.config.fileConfig(fname=CONFIG_FILE,disable_existing_loggers=False)
logger = logging.getLogger(__name__)

supportedBands = ['B04', 'B08']
extractedFilesPath = "C:\\Users\\girij\\PycharmProjects\\other resources\\gis data\\sentinelsat\\extracted\\"

merged_file_directory = "C:\\Users\\girij\\PycharmProjects\\other resources\\gis data\\sentinelsat\\merged\\"
merged_original = merged_file_directory + "original\\"
merged_reprojected = merged_file_directory + "reprojected\\"
jp2_file_name = "merged_file_band02.jp2"

masked_file_directory = "C:\\Users\\girij\\PycharmProjects\\other resources\\gis data\\sentinelsat\\masked\\"

merger = Merger()
bands = Utils.get_imagery_band(supported_bands=supportedBands, directory=extractedFilesPath)
logger.debug(f'{bands}')

# test input for merge_bands
# bands = {'B04': ['C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2A_MSIL2A_20200113T045131_N0213_R076_T45QVG_20200113T071528.SAFE\\GRANULE\\L2A_T45QVG_A023811_20200113T045526\\IMG_DATA\\R10m\\T45QVG_20200113T045131_B04_10m.jp2', 'C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2B_MSIL2A_20200115T044129_N0213_R033_T45QWG_20200115T081323.SAFE\\GRANULE\\L2A_T45QWG_A014931_20200115T044505\\IMG_DATA\\R10m\\T45QWG_20200115T044129_B04_10m.jp2', 'C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2B_MSIL2A_20200115T044129_N0213_R033_T45QXG_20200115T081323.SAFE\\GRANULE\\L2A_T45QXG_A014931_20200115T044505\\IMG_DATA\\R10m\\T45QXG_20200115T044129_B04_10m.jp2', 'C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2B_MSIL2A_20200115T044129_N0213_R033_T45RWH_20200115T081323.SAFE\\GRANULE\\L2A_T45RWH_A014931_20200115T044505\\IMG_DATA\\R10m\\T45RWH_20200115T044129_B04_10m.jp2', 'C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2B_MSIL2A_20200115T044129_N0213_R033_T45RXH_20200115T081323.SAFE\\GRANULE\\L2A_T45RXH_A014931_20200115T044505\\IMG_DATA\\R10m\\T45RXH_20200115T044129_B04_10m.jp2'], 'B08': ['C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2A_MSIL2A_20200113T045131_N0213_R076_T45QVG_20200113T071528.SAFE\\GRANULE\\L2A_T45QVG_A023811_20200113T045526\\IMG_DATA\\R10m\\T45QVG_20200113T045131_B08_10m.jp2', 'C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2B_MSIL2A_20200115T044129_N0213_R033_T45QWG_20200115T081323.SAFE\\GRANULE\\L2A_T45QWG_A014931_20200115T044505\\IMG_DATA\\R10m\\T45QWG_20200115T044129_B08_10m.jp2', 'C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2B_MSIL2A_20200115T044129_N0213_R033_T45QXG_20200115T081323.SAFE\\GRANULE\\L2A_T45QXG_A014931_20200115T044505\\IMG_DATA\\R10m\\T45QXG_20200115T044129_B08_10m.jp2', 'C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2B_MSIL2A_20200115T044129_N0213_R033_T45RWH_20200115T081323.SAFE\\GRANULE\\L2A_T45RWH_A014931_20200115T044505\\IMG_DATA\\R10m\\T45RWH_20200115T044129_B08_10m.jp2', 'C:\\Users\\girij\\PycharmProjects\\gis data\\sentinelsat\\extracted\\S2B_MSIL2A_20200115T044129_N0213_R033_T45RXH_20200115T081323.SAFE\\GRANULE\\L2A_T45RXH_A014931_20200115T044505\\IMG_DATA\\R10m\\T45RXH_20200115T044129_B08_10m.jp2']}
# merged_filedict = merger.merge_bands(bands_dictionary=bands, merged_file_path=merged_original)

shape_location = "Birbhum"
shape_identifier = "District"
shape_cutter = ShapeCutter()
shape = shape_cutter.get_shape(shape_location, shape_identifier)
shape_json = Utils.get_json_from_shape(shape)
masker = Masker()

# Test Data - merged_filedict
# merged_filedict = {'B04': 'merged-20200927154147-B04.jp2', 'B08': 'merged-20200927154450-B08.jp2'}

# masked_filename_dict = masker.mask(imagery_data=merged_filedict, shape_data=shape, merged_filepath=merged_original,
#             merged_reprojected=merged_reprojected, masked_filepath=masked_file_directory)

# Test Data - masked filedict
masked_filename_dict = {'B04': 'masked-20200927220047-B04.jp2', 'B08': 'masked-20200927220407-B08.jp2'}
# for supported_band in masked_filename_dict.keys():
#     Utils.plot_rasterio_imagery(masked_file_directory+masked_filename_dict[supported_band])

band_data_dict = {}
for supported_band in masked_filename_dict.keys():
    with rasterio.open(masked_file_directory+masked_filename_dict[supported_band],driver=Utils.get_driver_fromfile(masked_filename_dict[supported_band])) as dataset_reader:
        band_data_dict[supported_band] = dataset_reader.read()
        meta = dataset_reader.profile
    # dataset_reader = rasterio.open(masked_file_directory+masked_filename_dict[supported_band],
    # driver=Utils.get_driver_fromfile(masked_filename_dict[supported_band])) band_data_dict[supported_band] =
    # dataset_reader.read()

np.seterr(divide='ignore', invalid='ignore')
ndvi = (band_data_dict["B08"].astype(float) - band_data_dict["B04"].astype(float))/(band_data_dict["B08"]+band_data_dict["B04"])

print(type(ndvi))
print(ndvi.dtype)

show(ndvi)
print('hello')