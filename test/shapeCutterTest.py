import unittest

from processor.model import ShapeEntity
from processor.shapecutter import ShapeCutter


class ShapeCutterTest(unittest.TestCase):
    shape_location = "Birbhum"
    shape_identifier = "District"

    def test_get_shape_from_shapefile(self):
        # pass
        shape_entity: ShapeEntity = ShapeCutter.get_shape(self.shape_location, self.shape_identifier)
        self.assertIsInstance(shape_entity, ShapeEntity)


if __name__ == '__main__':
    unittest.main()
